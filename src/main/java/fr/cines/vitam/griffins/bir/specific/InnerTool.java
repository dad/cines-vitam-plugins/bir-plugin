/*******************************************************************************
 * Copyright French Prime minister Office/SGMAP/DINSIC/Vitam Program (2015-2019)
 *
 * contact.vitam@culture.gouv.fr
 *
 * This software is a computer program whose purpose is to implement a digital archiving back-office system managing
 * high volumetry securely and efficiently.
 *
 * This software is governed by the CeCILL 2.1 license under French law and abiding by the rules of distribution of free
 * software. You can use, modify and/ or redistribute the software under the terms of the CeCILL 2.1 license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the
 * successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 * that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
 * to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL 2.1 license and that you
 * accept its terms.
 *******************************************************************************/

package fr.cines.vitam.griffins.bir.specific;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cines.vitam.griffins.bir.Main;
import fr.cines.vitam.griffins.bir.pojo.Action;
import fr.cines.vitam.griffins.bir.status.ActionType;
import fr.cines.vitam.griffins.bir.status.AnalyseResult;

/**
 * Class encapsulating the tool used by the griffin.
 * <p>
 * Here Jhove java library.
 */
public class InnerTool {
	public static String urlRilApi = "http://kaolin.cines.fr:9000/ril/";

	private static final Logger logger = LoggerFactory.getLogger(InnerTool.class);

	private ResteasyClient resteasyClient;

	/**
	 * Instantiates a new inner tool.
	 *
	 * @throws Exception the exception
	 */
	public InnerTool(String urlBir) throws Exception {
		urlRilApi = urlBir;
		resteasyClient = new ResteasyClientBuilder().build();
	}

	private boolean isXML(String format) {
		if (format != null) {
			return format.matches("XML|xml");
		}
		return false;
	}

	private RawOutput doValidate(String filePath, String format) {
		String url = null;
		RawOutput rawoutput = null;
		if (!urlRilApi.endsWith("/")) {
			urlRilApi = urlRilApi + "/";
		}
		if(isXML(format)) {
			url = urlRilApi + "checkxml";
		} else {
			url = urlRilApi + "repinfo/" + format;
		}
		ResteasyWebTarget target = resteasyClient.target(url);
		if(isXML(format)) {
			target.property("Content-Type", MediaType.MULTIPART_FORM_DATA);

			MultipartFormDataOutput multipartFormDataOutput = new MultipartFormDataOutput();
			try {
				String fileName = Paths.get(filePath).getFileName().toString();
				multipartFormDataOutput.addFormData("uploadedFile", new FileInputStream(new File(filePath)), MediaType.MULTIPART_FORM_DATA_TYPE, fileName);
			} catch (FileNotFoundException e) {
				rawoutput = new RawOutput("bir", AnalyseResult.WRONG_FORMAT);
			}
			GenericEntity<MultipartFormDataOutput> genericEntity = new GenericEntity<MultipartFormDataOutput>(multipartFormDataOutput) {};

			Response response = target.request().post(Entity.entity(genericEntity, MediaType.MULTIPART_FORM_DATA));

			if (!response.getStatusInfo().equals(Status.OK)) {

				logger.info("uploadXmlFileToRIL:: Erreur lors de la récupération des schémas du fichier XML. xmlPath = [{}] url = [{}]", filePath, (String)response.getEntity());
				return new RawOutput("bir", AnalyseResult.WRONG_FORMAT);
			}
			//	    	List<Schema> schemas = response.readEntity(new GenericType<List<Schema>>() {});
			rawoutput = new RawOutput("bir", AnalyseResult.VALID_ALL);

		} else {
			System.out.println("appel de " + urlRilApi + "repinfo/" + format); 
			logger.info("appel de " + urlRilApi + "repinfo/" + format);
			Response response = target.request().get();
			if(response.getStatusInfo().equals(Status.OK)) {
				rawoutput = new RawOutput("bir", AnalyseResult.VALID_ALL);
				System.out.println("statusOK");
				return rawoutput;
			} 

			rawoutput = new RawOutput("bir", AnalyseResult.WRONG_FORMAT);

		}

		return rawoutput;

		/*
        // get the Jhove module for the specified format (puidType filter done at global level)
        String moduleName = PuidType.formatTypes.get(format);
        jhoveModule = jhoveBase.getModule(moduleName);

        // do Jhove call
        File file = new File(fileName);
        RepInfo info = new RepInfo(fileName);
        try {
            if (!jhoveBase.processFile(jhoveAPP, jhoveModule, false, file, info)) {
                info.setWellFormed(RepInfo.UNDETERMINED);
            }
        } catch (Exception e) {
            return new RawOutput(e);
        }

        if ((info.getValid() == RepInfo.TRUE)
                // special case for XML-hul module where undetermined
                //is when there's no DTD or schema
                || ((moduleName.equals("XML-hul") && (info.getValid() == RepInfo.UNDETERMINED)))) {
            result = new RawOutput(moduleName, AnalyseResult.VALID_ALL);
        } else if (info.getWellFormed() == RepInfo.TRUE) {
            result = new RawOutput(moduleName, AnalyseResult.WELL_FORMED);
        } else if ((info.getFormat() != null) && (!info.getFormat().isEmpty())) {
            result = new RawOutput(moduleName, AnalyseResult.NOT_VALID);
        } else {
            result = new RawOutput(moduleName, AnalyseResult.WRONG_FORMAT);
        }

        return result;*/
	}

	/**
	 * Apply the action in the inner tool for one file in given format.
	 * <p>
	 * This is the main class to adapt for a inner tool treating file by file
	 *
	 * @param action         the action
	 * @param inputFileName  the input file name
	 * @param format         the format
	 * @param outputFileName the output file name if relevant
	 * @return the raw output
	 */
	public RawOutput apply(Action action, String inputFileName, String format, String outputFileName, boolean debugFlag) {
		if (!action.getType().equals(ActionType.IDENTIFY)) {
			return new RawOutput(new Exception(Main.ID + " can only IDENTIFY"));
		}
		return doValidate(inputFileName, format);
	}
}
