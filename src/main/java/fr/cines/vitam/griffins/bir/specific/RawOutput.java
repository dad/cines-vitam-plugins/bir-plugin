/**
 * 
 */
package fr.cines.vitam.griffins.bir.specific;

import static fr.cines.vitam.griffins.bir.status.ActionType.ANALYSE;

import java.io.PrintWriter;
import java.io.StringWriter;

import fr.cines.vitam.griffins.bir.Main;
import fr.cines.vitam.griffins.bir.pojo.Action;
import fr.cines.vitam.griffins.bir.pojo.Input;
import fr.cines.vitam.griffins.bir.pojo.Output;
import fr.cines.vitam.griffins.bir.status.AnalyseResult;

/**
 * @author Florent MARCETEAU
 *
 */
public class RawOutput {
    /**
     * The Analyse result (specific variable).
     */
    public AnalyseResult analyseResult;
    /**
     * The Action (generic variable).
     */
    public Action action;
    /**
     * The Input (generic variable).
     */
    public Input input;
    /**
     * /**
     * The output file name (generic variable).
     */
    public String outputFileName;
    /**
     * The Error message (generic variable).
     */
    public String errorMessage;
    /**
     /**
     * The Execution context (generic variable).
     */
    public String executionContext;

    /**
     * Instantiates a new Raw output, the standard way using exception.
     *
     * @param exception the exception
     */
    public RawOutput(Exception exception) {
        this.executionContext = Main.ID;
        this.analyseResult = null;
        if (exception != null) {
            StringWriter sw = new StringWriter();
            exception.printStackTrace(new PrintWriter(sw));
            this.errorMessage = exception.getMessage() + "\n" + sw.toString();
        } else
            this.errorMessage = "";
    }

    /**
     * Instantiates a new Raw output, in the inner tool specific way.
     *
     * @param moduleName    the module name
     * @param analyseResult the analyse result
     */
    public RawOutput(String moduleName, AnalyseResult analyseResult) {
        this.executionContext = Main.ID + (moduleName != null ? "-" + moduleName : "");
        this.analyseResult = analyseResult;
        this.errorMessage = null;
    }

    /**
     * Sets context (which Action on which Input).
     *
     * @param input  the input
     * @param action the action
     * @return the context
     */
    public RawOutput setContext(Input input, Action action) {
        this.input = input;
        this.action = action;
        return this;
    }

    /**
     * Process inner tool RawOutput to griffin standard Output.
     *
     * @param debug the debug
     * @return the output
     * @throws RuntimeException the runtime exception
     */
    public Output postProcess(boolean debug) throws RuntimeException {
        switch (action.getType()) {
        	case IDENTIFY:
            case ANALYSE:
                Output result;
                if (errorMessage != null) {
                    return debug
                        ? Output.error(input, action.getType(), errorMessage, executionContext)
                        : Output.error(input, action.getType());
                }

                result = Output.ok(input, null, action.getType());
                result.setAnalyseResult(analyseResult);
                return result;
            default:
                throw new IllegalStateException(String.format("Cannot post process data from action of type %s only %s is supported.", action.getType(), ANALYSE));
        }
    }

}
