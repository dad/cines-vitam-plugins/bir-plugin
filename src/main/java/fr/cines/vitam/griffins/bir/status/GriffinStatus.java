package fr.cines.vitam.griffins.bir.status;

public enum GriffinStatus {
	OK(0),
	KO(1),
	WARNING(2);

	public final int code;

	GriffinStatus(int code) {
		this.code = code;
	}
}
