/*
 * Copyright CINES, 2020 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au
 * droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 * redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 * l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 * à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 * leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 * leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 * sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */
package fr.gouv.vitam.worker.core.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.gouv.vitam.common.SedaConstants;
import fr.gouv.vitam.common.json.JsonHandler;
import fr.gouv.vitam.common.logging.VitamLogger;
import fr.gouv.vitam.common.logging.VitamLoggerFactory;
import fr.gouv.vitam.common.model.IngestWorkflowConstants;
import fr.gouv.vitam.common.model.ItemStatus;
import fr.gouv.vitam.common.model.StatusCode;
import fr.gouv.vitam.common.model.VitamAutoCloseable;
import fr.gouv.vitam.processing.common.exception.ProcessingException;
import fr.gouv.vitam.processing.common.parameter.WorkerParameters;
import fr.gouv.vitam.worker.common.HandlerIO;
import fr.gouv.vitam.worker.core.handler.ActionHandler;
import fr.gouv.vitam.worker.core.plugin.pojo.ValidatorResult;
import fr.gouv.vitam.workspace.api.exception.ContentAddressableStorageNotFoundException;
import fr.gouv.vitam.workspace.api.exception.ContentAddressableStorageServerException;

/**
 * @author Florent MARCETEAU
 *
 */
public class BIRPlugin extends ActionHandler implements VitamAutoCloseable {
	private static final VitamLogger LOGGER = VitamLoggerFactory.getInstance(BIRPlugin.class);
	
	private static final String BIR_PLUGIN = "BIR_PLUGIN";

	private ResteasyClient resteasyClient;

	private final ObjectMapper mapper = new ObjectMapper();
	
	public ItemStatus execute(WorkerParameters workerParameters, HandlerIO handlerIO) {
        LOGGER.warn("===========================================================");
        LOGGER.warn("=================   BIR_PLUGIN         ........ =========== ");
        LOGGER.warn("===========================================================");

        // Add some event details data
        
        ItemStatus itemStatus = new ItemStatus(BIR_PLUGIN);
		boolean rilOk = false;
        List <ValidatorResult> validatorResultList = null;
        try {
        	final JsonNode jsonOG = (JsonNode) handlerIO.getInput(0);
        	final Object obj = handlerIO.getInput(1);
        	if(obj != null && obj instanceof File) {
            	try {
    				validatorResultList = mapper.readValue((File) obj, new TypeReference <List <ValidatorResult>>() {});
    			} catch (JsonParseException e) {
    				LOGGER.error("impossible d'analyser le fichier json {}.", ((File) obj).getAbsolutePath());
    			} catch (JsonMappingException e) {
    				LOGGER.error("impossible de mapper le fichier json {} sur la classe ValidatorResult.", ((File) obj).getAbsolutePath());
    			} catch (IOException e) {
    				LOGGER.error("impossible d'accéder au fichier {}.", ((File) obj).getAbsolutePath());
    			}
            }
        	LOGGER.info("JsonOG : " + jsonOG.textValue());
            final Map<String, String> objectIdToUri = getMapOfObjectsIdsAndUris(jsonOG);
            final JsonNode qualifiers = jsonOG.get(SedaConstants.PREFIX_QUALIFIERS);
            if (qualifiers != null) {
            	final List<JsonNode> versions = qualifiers.findValues(SedaConstants.TAG_VERSIONS);
            	if (versions != null && !versions.isEmpty()) {
            		for(final JsonNode versionsArray : versions) {
            			for (final JsonNode version : versionsArray) {
            				if (version.get(SedaConstants.TAG_PHYSICAL_ID) == null) {
            					File file = null;
            					try {
            						final String objectId = version.get(SedaConstants.PREFIX_ID).asText();
            						String filePath = objectIdToUri.get(objectId);
            						file = loadFileFromWorkspace(handlerIO, filePath);
            						for (ValidatorResult vr : validatorResultList) {
            							if(vr.getFilename().equals(filePath)) {
                							rilOk = checkRil(file, vr.getFormatId());
                							if(rilOk) {
                								itemStatus.increment(StatusCode.OK);
                    							return itemStatus;
                							} else {
                    							final ObjectNode object = JsonHandler.createObjectNode();
                    		                    object.put(SedaConstants.EV_DET_TECH_DATA, "Le format spécifié " + vr.getFormatId() + " du fichier " + file.toString() + " n'est pas dans la BIR ");
                								itemStatus.increment(StatusCode.KO);
                    				        	itemStatus.setEvDetailData(JsonHandler.unprettyPrint(object));
                    				        	LOGGER.warn("Le format spécifié {} du fichier {} n'est pas dans la BIR", vr.getFormatId(), file.toString());
                    				        	return new ItemStatus(BIR_PLUGIN).setItemsStatus(itemStatus.getItemId(), itemStatus);
                								
                							}
                						}
            						}
            					} catch (Exception e) {
									LOGGER.error("Une erreur s'est produite {}", e.getMessage());
								}
            				}
            			}
            		}
            	}
            }
        } catch (Exception e) {
        	LOGGER.error("erreur lors de l'execution du plugin BIR : " + e.getMessage() ) ;
		}
        return itemStatus;
    }

	private Map<String, String> getMapOfObjectsIdsAndUris(JsonNode jsonOG) {
        final Map<String, String> binaryObjectsToStore = new HashMap<>();

        // Filter on objectGroup objects ids to retrieve only binary objects
        // informations linked to the ObjectGroup
        final JsonNode work = jsonOG.get(SedaConstants.PREFIX_WORK);
        final JsonNode qualifiers = work.get(SedaConstants.PREFIX_QUALIFIERS);

        if (qualifiers == null) {
            return binaryObjectsToStore;
        }

        final List<JsonNode> versions = qualifiers.findValues(SedaConstants.TAG_VERSIONS);
        if (versions == null || versions.isEmpty()) {
            return binaryObjectsToStore;
        }
        for (final JsonNode version : versions) {
            for (final JsonNode binaryObject : version) {
                if (binaryObject.get(SedaConstants.TAG_PHYSICAL_ID) == null) {
                    binaryObjectsToStore.put(binaryObject.get(SedaConstants.PREFIX_ID).asText(),
                        binaryObject.get(SedaConstants.TAG_URI).asText());
                }
            }
        }
        return binaryObjectsToStore;
    }
	
	private boolean checkRil(File file, String format) {
		Properties prop = new Properties();
		InputStream resources = BIRPlugin.class.getResourceAsStream("/bir.properties");
		try {
			prop.load(resources);
		} catch (IOException e1) {
			LOGGER.error("impossible d'accéder au fichier bir.properties");
			return false;
		}
		String filePath = file.getAbsolutePath();
		resteasyClient = new ResteasyClientBuilder().build();
		String urlRilApi = prop.getProperty("fr.cines.pac.ril.ApiUrl");
		String url = null;
		if (!urlRilApi.endsWith("/")) {
			urlRilApi = urlRilApi + "/";
		}
		if(isXML(format)) {
			url = urlRilApi + "checkxml";
		} else {
			url = urlRilApi + "repinfo/" + format;
		}
		ResteasyWebTarget target = resteasyClient.target(url);
		if(isXML(format)) {
			target.property("Content-Type", MediaType.MULTIPART_FORM_DATA);

			MultipartFormDataOutput multipartFormDataOutput = new MultipartFormDataOutput();
			try {
				String fileName = Paths.get(filePath).getFileName().toString();
				multipartFormDataOutput.addFormData("uploadedFile", new FileInputStream(new File(filePath)), MediaType.MULTIPART_FORM_DATA_TYPE, fileName);
			} catch (FileNotFoundException e) {
				LOGGER.error("impossible d'accéder au fichier xml ");
			}
			GenericEntity<MultipartFormDataOutput> genericEntity = new GenericEntity<MultipartFormDataOutput>(multipartFormDataOutput) {};

			Response response = target.request().post(Entity.entity(genericEntity, MediaType.MULTIPART_FORM_DATA));

			if (!response.getStatusInfo().equals(Status.OK)) {

				LOGGER.info("uploadXmlFileToRIL:: Erreur lors de la récupération des schémas du fichier XML. xmlPath = [{}] url = [{}]", filePath, (String)response.getEntity());
				return false;
			}
			return true;

		} else {
			LOGGER.info("appel de " + urlRilApi + "repinfo/" + format);
			Response response = target.request().get();
			if(response.getStatusInfo().equals(Status.OK)) {
				return true;
			} 

			return false;

		}
	}
	
	
	private File loadFileFromWorkspace(HandlerIO handlerIO, String filePath)
            throws ProcessingException {
            try {
                return handlerIO.getFileFromWorkspace(IngestWorkflowConstants.SEDA_FOLDER + "/" + filePath);
            } catch (final IOException e) {
                LOGGER.debug("Error while saving the file", e);
                throw new ProcessingException(e);
            } catch (ContentAddressableStorageNotFoundException | ContentAddressableStorageServerException e) {
                LOGGER.debug("Workspace Server Error", e);
                throw new ProcessingException(e);
            }
        }

	private boolean isXML(String format) {
		if (format != null) {
			return format.matches("XML|xml");
		}
		return false;
	}

}
