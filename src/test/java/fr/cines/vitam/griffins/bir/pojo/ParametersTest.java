/*
 * Copyright CINES, 2020 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au
 * droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 * redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 * l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 * à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 * leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 * leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 * sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */
package fr.cines.vitam.griffins.bir.pojo;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cines.vitam.griffins.bir.status.ActionType;

/**
 * @author Florent MARCETEAU
 *
 */
public class ParametersTest {
	private final ObjectMapper mapper = new ObjectMapper();

	@Test
	public void testSerialize() throws JsonGenerationException, JsonMappingException, IOException {
		Parameters param = new Parameters();
		param.setId("identifiant batch");
		param.setRequestId("id requete vitam");
		
		Action action = new Action();
		action.setType(ActionType.IDENTIFY);
		Values values = new Values();
		values.setArgs(Arrays.asList("toto.pdf", "pdf\\1.4"));
		action.setValues(values);
		
		param.setActions(Arrays.asList(action));
		Input input = new Input("toto.pdf", "pdf/1.4");
		param.setInputs(Arrays.asList(input));
		mapper.writeValue(new File("parameters.json"), param);
	}

}
